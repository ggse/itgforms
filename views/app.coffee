$ ->

  update_seconds = do ->
    countdown = 10
    ->
      if countdown-- is 0
        location.href = document.referrer
      countdown = 0 if countdown < 1
      $('#seconds').text countdown

  interval = setInterval update_seconds, 1000

  $('#stay_here').click (e) ->
    e.preventDefault()
    clearInterval interval
    $(@).attr('href', null).addClass 'disabled'
    $('#countdown').text 'Redirect cancelled'
  

  $('#stay_here').after('<p id=countdown>Redirecting in <span id=seconds>10</span> seconds</p>')

