require 'rubygems'
require 'bundler/setup'
require 'tempfile'

UPSTART=<<END
start on runlevel [2345]
stop on runlevel [!2345]

env HOME=%{home}
chdir %{pwd}
exec %{passenger} start --environment production --user %{user}
respawn
END

ENV['RACK_ENV'] ||= 'development'

def development?
  ENV['RACK_ENV'] == 'development'
end

def production?
  ENV['RACK_ENV'] == 'production'
end

desc 'Start the server'
task :start do
  `passenger start --daemon`
end

desc 'Stop the server'
task :stop do
  `passenger stop`
end

desc 'Restart the server'
task restart: [:stop, :start]

desc 'Run all installation tasks'
task install: :upstart

desc 'Make RVM wrapper for passenger bin'
task :rvm do
  `rvm wrapper #{`rvm current`.chomp} --no-prefix passenger`
end

desc 'Create an upstart job to start the app on boot (requires Ubuntu, RVM, and your sudo password)'
task upstart: :rvm do
  Rake::Task['rvm'].invoke
  home = `echo $HOME`.chomp
  user = `whoami`.chomp
  passenger = "#{home}/.rvm/bin/passenger"
  pwd = `pwd`.chomp
  upstart = UPSTART % {
    home: home,
    user: user,
    passenger: passenger,
    pwd: pwd
  }
  tmp = Tempfile.new('itgforms_pstart')
  tmp.write upstart
  tmp.flush
  `sudo cp #{tmp.path} /etc/init/itgforms.conf`
  tmp.unlink
  `sudo chmod 644 /etc/init/itgforms.conf`
end
