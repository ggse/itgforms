require 'sinatra/base'
require 'sinatra/reloader'
require 'mail'
require 'sass'

class ITGForms < Sinatra::Base

  configure :development do
    register Sinatra::Reloader
  end

  def development?
    ENV['RACK_ENV'] == 'development'
  end

  def conf
    @@conf ||= YAML.load_file 'config/mail.yml'
  end

  post '/' do

    host, port, user, password = conf['host'], conf['port'], conf['user'], conf['password']

    smtp_conn = Net::SMTP.new host, port
    smtp_conn.enable_starttls_auto
    smtp_conn.start host, user, password, :plain
    Mail.defaults do
      delivery_method :smtp_connection, { :connection => smtp_conn }
    end
 
    # Build subject and body of email
    #
    subject = "ITG Forms: [#{params[:subject]}]"
    from = params[:from]
    to = params[:to]
    from = 'noreply@education.ucsb.edu' if from == nil or from.blank?
    max_key_length = params.keys.max{|a, b| a.length <=> b.length}.length
    body = params.collect do |k,v|
      "%s\n%-#{max_key_length+1}s %s\n" % ['-'*25, "#{k}:", v]
    end.join

    # Send mail
    #
    #mail = Mail.deliver do
    mail = Mail.deliver do
      to      to
      from    from
      subject subject
      body    body
    end

    # Output a simple webpage summarizing displaying the sent mail. This should
    # probably be removed and we should instead display a success message and
    # redirect back to the referring page?
    #
    @body = body
    @referer = request.referer
    slim :success
  end

  get '/' do
    <<-OUT
    <!DOCTYPE html>
    <title>Sample Form</title>
    <form action=. method=post>
      <fieldset>
        <label for=from>From: <input id=from name=from> <br>
        <label for=to>To: <input id=to name=to> <br>
        <label for=subject>Subject: <input id=subject name=subject> <br>
        <label for=notes>Notes: <br> <textarea id=notes name=notes></textarea> <br>
        <input type=submit value=Send>
    </form>
    OUT
  end if development?

  get '/app.js' do
    coffee :app
  end

  get '/style.css' do
    scss :style
  end
end
