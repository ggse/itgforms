# itgforms #

## Download and Setup ##

    git clone https://bitbucket.org/ggse/itgforms
    cd itgforms
    bundle
    rake install

### Config File ###

Edit `mail.yml` to fit your environment.

## Running ##

### Production ###

    sudo start itgforms

### Development ###

    rake start

## Copyright ##

Copyright (c) 2012 Justin Force

Licensed under the MIT License
