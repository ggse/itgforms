require 'rubygems'
require 'bundler'

Bundler.require

require 'sinatra'
require File.join(File.dirname(__FILE__), './itgforms')

disable :run

map '/itgforms' do
  run ITGForms
end
